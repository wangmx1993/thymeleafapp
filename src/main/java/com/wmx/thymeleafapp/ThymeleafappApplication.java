package com.wmx.thymeleafapp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.web.client.RestTemplate;

/**
 * 1、@EnableRetry + @Retryable 注解优雅的实现循环重试功能
 *
 * @author wangmaoxiong
 * @version 1.0
 * @date 2020/5/10 9:50
 * {@link MapperScan} 指定 MyBatis 扫描的 Mapper 接口目录.
 */
@EnableRetry
@SpringBootApplication
@MapperScan(value = {"com.wmx.thymeleafapp.mapper"})
public class ThymeleafappApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThymeleafappApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
