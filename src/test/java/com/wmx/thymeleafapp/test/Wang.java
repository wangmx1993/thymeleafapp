package com.wmx.thymeleafapp.test;

import io.swagger.models.auth.In;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/12/17 11:18
 */
public class Wang {

    @Test
    public void test(){
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        List<Integer> result = list.stream()
                .mapToInt(x -> x)
                .map(x -> ++x)
                .boxed()
                .collect(Collectors.toCollection(ArrayList::new));

        System.out.println(result);
    }

}
